DEBUG = false;
threshold = 150;
video = "";
k = 1;


var camWidth = 640;//1280;
var camHeight = 480;//960;
SUMarkerCount = [];
var x=0,y=0;

function main(){
  video = document.getElementById('video');
  var media = navigator.mozGetUserMedia({video:true},function(localMediaStream){
    var cam = window.URL.createObjectURL(localMediaStream);
    video.src = cam;
    video.play();
    init();
  },function(err){
    console.log(err);
  });

  window.onkeydown = function(e){

    console.log(e.keyCode);
    switch(e.keyCode){

      case 68:
        DEBUG = !DEBUG;
      case 83:
        document.getElementById('video').style.display = "";
      case 72:
        document.getElementById('video').style.display = "none";
      default:
        console.log(e.keyCode);
        break;
    }

  };
}

function initCanvas(_cvs){
  _cvs.width = camWidth;
  _cvs.height = camHeight;

}

function init(){
  cvs = document.getElementById('cvs');
  dcvs = document.getElementById('debugCanvas');
  result = document.getElementById("result");
  initCanvas(cvs);
  dcvs.width = screen.width;
  dcvs.height = screen.height;
  initCanvas(result);
  dctx = dcvs.getContext('2d');
  rctx  = result.getContext('2d');
  raster = new NyARRgbRaster_Canvas2D(cvs);
  param = new FLARParam(camWidth, camHeight);
  detector = new FLARMultiIdMarkerDetector(param, threshold);
  ctx = cvs.getContext('2d');
  window.setInterval(loop,100);
} 
function loop(){
  if(!DEBUG)dctx.clearRect(0,0,640,480);
  SUMarkerCount = [];
  ctx.drawImage(video,0,0,camWidth,camHeight);
  var data = ctx.createImageData(camWidth,camHeight);
  //rctx.putImageData(data,camWidth,camHeight);
  detector.setContinueMode(true);
  cvs.changed= true;
  var markerCount = detector.detectMarkerLite(raster, threshold);
  //res.getZXYAngle(a);
  /* 
   * console.log(
   Math.floor(a.x*180/Math.PI)+" , " +
   Math.floor(a.y*180/Math.PI)+" , " + 
   Math.floor(a.z*180/Math.PI));
   */ 
  //console.log("x: "+res.m03*k+", y: "+res.m13*k+",z: "+res.m23*k);
  if(SUMarkerCount.length>3){
    applyHomography(calcHomo(sortMarkerArray(SUMarkerCount))); 
  }
}
function sortMarkerArray(a){
  var b = a.sort(function(a,b){
    if(a.x > b.x) return 1;
    else return -1;
  });
  var c  = [];
  if(b[0].y >  b[1].y){
    c[0] = clone(b[1]);
    c[3] = clone(b[0]);
  }else {
    c[3] = clone(b[1]);
    c[0] = clone(b[0]);
  }

  if(b[2].y > b[3].y){
    c[1] = clone(b[3]);
    c[2] = clone(b[2]);
  }else {
    c[2] = clone(b[3]);
    c[1] = clone(b[2]);
  }
  /*
     var str = "                                    ";
     for(i in c){
     str += c[i].x +" , "+c[i].y+ "--";
     }
     console.log(str);
     */
  return c;

}

function clone(a){
  var b = {};
  b['x'] = a.x;
  b['y'] = a.y;
  return b;
}


function calcHomo(src){
  var M = [];
  var V = [];
  d = src;
  x = src[0].x;
  y = src[0].y;

  s = [//TL TR DR DL
  {x:0,y:0},
  {x:screen.availWidth,y:0},
  {x:screen.availWidth,y:screen.availHeight},
  {x:0,y:screen.availHeight}
  ];

  debug(s);
  debug(d);

  for(var i=0;i<4;i++){ 
    M.push([s[i].x, s[i].y, 1,0,0,0, -s[i].x * d[i].x, -s[i].y * d[i].x]);
    M.push([0,0,0, s[i].x, s[i].y, 1, -s[i].x * d[i].y, -s[i].y * d[i].y]);
    V.push(d[i].x);
    V.push(d[i].y);
  }

  console.log(s);
  console.log(d);
  var ret = $M(M).inverse().multiply($V(V)).elements;
  for(var i=0;i<8;i++){
    ret[i] = Math.floor(ret[i]*100000)/100000;
  }
  H = [[ret[0],ret[4],ret[1]],
    [ret[5],ret[2],ret[6]],
    [ret[3],ret[7],1]];
  //var H = [[1,0,0],[0,1,0],[0,0,1]];
  debug(applyHomographyToPoints(d));
  return H; 
}

function applyHomographyToPoints(a){
  var r = [];
  for(var i=0;i<a.length;i++)console.log($M(H).x($V([a[i].x,a[i].y,0])).inspect());
  return r;
}

function applyHomography(r){
  //console.log($M(H).multiply($V([x,y,1])).inspect());
  img = document.getElementById("video");
  img.width=screen.availWidth+x;
  img.height=screen.availHeight+y;
  var m = "perspective(1px)scaleZ(-1)translateZ(-1px)translateX(-"+x+"px)translateY(-"+0+"px)matrix3d("+
    r[0][0]+","+r[0][1]+","+r[0][2]+",0,"+
    r[1][0]+","+r[1][1]+","+r[1][2]+",0,"+
    r[2][0]+","+r[2][1]+",1,0,0,0,0,1)translateZ(1px)";

  console.log(m);
  //img.style.transform = m;
}


/*  debug Util functions */

function debug(array){
  _debug(array,dctx);
}
function _debug(array,c){
  var colorList = ["red","green","blue","yellow"];
  for(var i=0;i<4;i++){
    c.fillStyle = colorList[i];
    c.fillRect(array[i].x-20,array[i].y-20,40,40);
    strokeCoordinate(array[i],c);
  }
  strokePoints(array);
}

function strokeCoordinate(p,ctx){
  var str = "("+p.x+" , "+p.y+")";
  ctx.fillText(str,p.x+30,p.y);
}

function strokePoints(array){
  ctx.beginPath();
  ctx.moveTo(array[0].x,array[0].y);
  ctx.lineTo(array[1].x,array[1].y);
  ctx.lineTo(array[2].x,array[2].y);
  ctx.lineTo(array[3].x,array[3].y);
  ctx.lineTo(array[0].x,array[0].y);
  ctx.stroke();
}

function test(){
  testArray = [
  {x:0,y:0},
  {x:640,y:0},
  {x:640,y:480},
  {x:0,y:480}
  ];  
  _debug(testArray,rctx);
  /* 
     testArray = [//TL TR DR DL
     {x:0,y:0},
     {x:screen.width,y:0},
     {x:screen.width,y:screen.height},
     {x:0,y:screen.height}
     ];*/
  var H = calcHomo(sortMarkerArray(testArray));
  applyHomography(H);
}

